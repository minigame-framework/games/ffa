package com.rast.ffa;

import com.rast.gamecore.GameCore;
import com.rast.gamecore.GameInstance;
import com.rast.gamecore.GameStatus;
import com.rast.gamecore.GameWorld;
import com.rast.gamecore.scores.ScoreFunction;
import com.rast.gamecore.scores.ScoreManager;
import com.rast.gamecore.util.BroadcastWorld;
import com.rast.gamecore.util.CleanPlayer;
import com.rast.gamecore.util.Region;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scheduler.BukkitTask;

import java.util.*;

public class FFAInstance extends GameInstance {

    // variables
    private final int maxPlayers;
    private final int minPlayers;

    // bossbar
    BossBar bossBar;

    // Player groups
    private final Set<Player> waitingPlayers = new HashSet<>();
    private  final Set<Player> gamePlayers = new HashSet<>();
    private  final Set<Player> respawnPlayers = new HashSet<>();
    private  final Set<Player> deadPlayers = new HashSet<>();

    // game flags
    private boolean gracePeriod = false;
    private boolean gameStarted = false;
    private boolean gameEnded = false;
    private boolean pvpEnabled = false;

    // player lives count
    private  final HashMap<Player, Integer> playerLives = new HashMap<>();

    // start timer and grace timer settings
    private long startTimerCount = 0;
    private long gracePeriodTime = 0;
    private BukkitTask startCounter;
    private BukkitTask gracePeriodCounter;

    // respawn timers
    private  final HashMap<Player, RespawnTimer> respawnTimers = new HashMap<>();

    // score tallies
    private  final HashMap<Player, Integer> playerKills = new HashMap<>();
    private  final HashMap<Player, Integer> playerDeaths = new HashMap<>();

    // despawn timer object
    private BukkitTask worldDespawnTimer;

    // get the variables for the game
    public FFAInstance(GameWorld gameWorld) {
        super(gameWorld);
        maxPlayers = FFA.getSettings().getMapConfig(getGameWorld().getMap()).getMaxPlayers();
        minPlayers = FFA.getSettings().getMapConfig(getGameWorld().getMap()).getMinPlayers();
        bossBar = Bukkit.createBossBar(ChatColor.YELLOW + "Waiting for players...", BarColor.YELLOW, BarStyle.SOLID);
        bossBar.setProgress(1.0);
    }

    // add player
    public void addPlayer(Player player) {
        // first ensure that the game is waiting open and the match does not have the player
        if (getGameWorld().getStatus() == GameStatus.WAITING_OPEN && !hasPlayer(player)) {
            // send the player to the game
            waitPlayerPrep(player);
            bossBar.addPlayer(player);
            playerLives.put(player, 3);
            BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GRAY + player.getName()
                    + " has joined the match. " + ChatColor.DARK_GRAY + '(' + getPlayerCount() + '/' + maxPlayers + ')');
            startCountdown();
        }
        worldStatusRefresh();
    }

    public void removePlayer(Player player) {
        if (hasPlayer(player)) {
            // remove the tags that may have been assigned to the player
            waitingPlayers.remove(player);
            gamePlayers.remove(player);
            deadPlayers.remove(player);
            respawnPlayers.remove(player);
            bossBar.removePlayer(player);
            playerLives.remove(player);
            BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GRAY + player.getName() + " has left the match.");
            if (!isGameReady()) {
                stopStartCountdown();
            }
            gameWinCheck();
            FFA.getKits().removePlayerKit(player);
            worldStatusRefresh();
            saveScoreTallies(player);
            if (respawnTimers.containsKey(player)) {
                respawnTimers.remove(player).stopTask();
            }
        }
    }

    // prep the player for waiting
    private void waitPlayerPrep(Player player) {
        gamePlayers.remove(player);
        deadPlayers.remove(player);
        waitingPlayers.add(player);
        respawnPlayers.remove(player);
        player.setGameMode(GameMode.ADVENTURE);
        Location loc = FFA.getSettings().getMapConfig(getGameWorld().getMap()).getGameSpawn();
        loc.setWorld(getGameWorld().getBukkitWorld());
        player.teleport(loc);
        CleanPlayer.cleanAll(player);
        player.getInventory().addItem(FFA.getKits().getKitMenuItem());
    }

    // prep the player for the game
    private void gamePlayerPrep(Player player) {
        deadPlayers.remove(player);
        waitingPlayers.remove(player);
        gamePlayers.add(player);
        respawnPlayers.remove(player);
        player.setGameMode(GameMode.SURVIVAL);
        player.teleport(getRandomSpawn());
        CleanPlayer.cleanAll(player);
        FFA.getKits().equipKit(player);
        gameWinCheck();
    }

    // prep the player for the game
    private void respawnPlayerPrep(Player player) {
        deadPlayers.remove(player);
        waitingPlayers.remove(player);
        gamePlayers.remove(player);
        respawnPlayers.add(player);
        player.setGameMode(GameMode.SPECTATOR);
        Location loc = FFA.getSettings().getMapConfig(getGameWorld().getMap()).getGameSpawn();
        loc.setWorld(getGameWorld().getBukkitWorld());
        player.teleport(loc);
        CleanPlayer.cleanAll(player);
        respawnTimers.put(player, new RespawnTimer(player, 5, () -> {
            if (!gameEnded) {
                gamePlayerPrep(player);
            }
        }));
    }

    // prep the player for spectate
    private void spectatePlayerPrep(Player player) {
        gamePlayers.remove(player);
        waitingPlayers.remove(player);
        deadPlayers.add(player);
        respawnPlayers.remove(player);
        player.setGameMode(GameMode.SPECTATOR);
        Location loc = FFA.getSettings().getMapConfig(getGameWorld().getMap()).getGameSpawn();
        loc.setWorld(getGameWorld().getBukkitWorld());
        player.teleport(loc);
        CleanPlayer.cleanAll(player);
    }

    // kill a player
    public void killPlayer(Player player, Player killer) {
        int lives = playerLives.get(player);
        lives--;
        playerLives.put(player, lives);
        playerDeaths.compute(player, (k, v) -> (v == null) ? 1 : v+1);
        for (PotionEffect effect : player.getActivePotionEffects()) {
            player.removePotionEffect(effect.getType());
        }
        if (playerLives.get(player) > 0) {
            if (killer != null) {
                BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.RED + player.getName() + " has been killed by " + killer.getName() + "! " + lives + " lives left.");
                playerKills.compute(killer, (k, v) -> (v == null) ? 1 : v+1);
            } else {
                BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.RED + player.getName() + " has been killed! " + lives + " lives left.");
            }
            respawnPlayerPrep(player);
        } else {
            if (killer != null) {
                BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.RED + player.getName() + " has been eliminated by " + killer.getName() + '!');
                playerKills.compute(killer, (k, v) -> (v == null) ? 1 : v+1);
            } else {
                BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.RED + player.getName() + " has been eliminated!");
            }
            BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.ENTITY_ZOMBIE_DEATH, 1.65f);
            player.sendTitle(ChatColor.RED + "Eliminated!", null, 0, 20, 5);
            spectatePlayerPrep(player);
            GameCore.getScoreManager().modifyScore(player, FFA.getFFAGame(), "Loses", 1, ScoreFunction.ADD);
            player.sendMessage(ChatColor.DARK_GRAY + "You had " + ((playerKills.get(player) == null)? 0:playerKills.get(player))
                    + " kills and " + ((playerDeaths.get(player) == null)? 0:playerDeaths.get(player)) + " deaths.");
        }
        BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.BLOCK_NOTE_BLOCK_BELL, 1.65f);
        gameWinCheck();
    }

    // check to see if this map has a player
    public boolean hasPlayer(Player player) {
        return waitingPlayers.contains(player) || gamePlayers.contains(player) || deadPlayers.contains(player) || respawnPlayers.contains(player);
    }

    public int getPlayerCount() {
        return waitingPlayers.size() + deadPlayers.size() + gamePlayers.size() + respawnPlayers.size();
    }

    private boolean isGameReady() {
        return minPlayers <= waitingPlayers.size();
    }

    private void startCountdown() {
        // end if game has started or the game is not ready
        if (!isGameReady() || gameStarted) {
            return;
        }
        // do not start the counter if the counter is already running
        if (startCounter != null && !startCounter.isCancelled()) {
            return;
        }
        // we can finally start the counter
        startTimerCount = FFA.getSettings().getGameCountdown();
        BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "There are enough players to start the match.");
        BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.BLOCK_DISPENSER_FAIL, 2);
        startCounter = Bukkit.getScheduler().runTaskTimer(FFA.getPlugin(), () -> {
            if (startTimerCount == 0 && isGameReady()) {
                // timer has reached 0, it is show time
                startGraceCountdown();
                gameStarted = true;
                worldStatusRefresh();
                bossBar.setVisible(false);
                stopStartCountdown();
                return;
            } else if (startTimerCount == 0) {
                // if the game was not ready and the count is at 0 we want to cancel
                BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "Countdown was aborted because there was not enough players in the game!");
                bossBar.setTitle(ChatColor.YELLOW + "Waiting for players...");
                bossBar.setProgress(1.0);
                stopStartCountdown();
                return;
            }
            if (startTimerCount == 1) {
                bossBar.setTitle(ChatColor.YELLOW + "Starting game in " + ChatColor.RED + startTimerCount + ChatColor.YELLOW + " second...");
                BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "Game starting in " + ChatColor.RED + startTimerCount + ChatColor.GOLD + " second.");
                BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.BLOCK_NOTE_BLOCK_BELL, 1);
            } else if (startTimerCount <= 10) {
                bossBar.setTitle(ChatColor.YELLOW + "Starting game in " + ChatColor.RED + startTimerCount + ChatColor.YELLOW + " seconds...");
                BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "Game starting in " + ChatColor.RED + startTimerCount + ChatColor.GOLD + " seconds.");
                BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.BLOCK_NOTE_BLOCK_BELL, 1);
            } else {
                bossBar.setTitle(ChatColor.YELLOW + "Starting game in " + ChatColor.RED + startTimerCount + ChatColor.YELLOW + " seconds...");
            }
            bossBar.setProgress((double) startTimerCount / 30);
            if (waitingPlayers.size() >= maxPlayers && startTimerCount > 10) {
                startTimerCount = FFA.getSettings().getGameCountdownFast();
                BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "Game full! Starting match in " + startTimerCount + 's');
                BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.BLOCK_DISPENSER_FAIL, 2);
            } else {
                startTimerCount--;
            }
        }, 0, 20);
    }

    // stop the starter countdown
    private void stopStartCountdown() {
        if (startCounter != null && !startCounter.isCancelled()) {
            startCounter.cancel();
            if (!isGameReady() && !gracePeriod) {
                BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "Countdown was aborted because there was not enough players in the game!");
                BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.BLOCK_DISPENSER_FAIL, 2);
            }
            bossBar.setTitle(ChatColor.YELLOW + "Waiting for players...");
            bossBar.setProgress(1.0);
        }
    }

    // start the grace period
    public void startGraceCountdown() {
        gracePeriod = true;
        BroadcastWorld.broadcastSound(getGameWorld().getBukkitWorld(), Sound.BLOCK_NOTE_BLOCK_BELL, 2);
        // move players into the arena
        Set<Player> playersToMove = new HashSet<>(getWaitingPlayers());
        for (Player playerToMove : playersToMove) {
            gamePlayerPrep(playerToMove);
        }
        gracePeriodTime = FFA.getSettings().getGracePeriod();

        // delay 20 ticks before counting the grace period
        gracePeriodCounter = Bukkit.getScheduler().runTaskTimer(FFA.getPlugin(), () -> {
            if (gracePeriodTime == 0) {
                for (Player gamePlayer : gamePlayers) {
                    gamePlayer.sendTitle(ChatColor.GOLD + "GO!", "", 0, 10, 5);
                    gamePlayer.playSound(gamePlayer.getLocation(), Sound.BLOCK_NOTE_BLOCK_BELL, SoundCategory.MASTER, Integer.MAX_VALUE, 2);
                }
                startGame();
                stopGraceCountdown();
                return;
            } else if (gracePeriodTime == 1) {
                for (Player gamePlayer : gamePlayers) {
                    gamePlayer.sendTitle(ChatColor.GOLD + "PVP Enables In", ChatColor.RED.toString() + gracePeriodTime + ChatColor.GOLD + " second", 0, 20, 5);
                    gamePlayer.playSound(gamePlayer.getLocation(), Sound.BLOCK_NOTE_BLOCK_BELL, SoundCategory.MASTER, Integer.MAX_VALUE, 1);
                }
            } else {
                for (Player gamePlayer : gamePlayers) {
                    gamePlayer.sendTitle(ChatColor.GOLD + "PVP Enables In", ChatColor.RED.toString() + gracePeriodTime + ChatColor.GOLD + " seconds", 0, 20, 5);
                    gamePlayer.playSound(gamePlayer.getLocation(), Sound.BLOCK_NOTE_BLOCK_BELL, SoundCategory.MASTER, Integer.MAX_VALUE, 1);
                }
            }
            gracePeriodTime--;
        }, 20, 20);
    }

    // Stop the grace period counter
    private void stopGraceCountdown() {
        gracePeriodCounter.cancel();
    }

    // the countdown at the end of the game until the game closes
    private void endGameCountdown() {
        BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.BLUE + "Match is closing in 10 seconds. Use /leave to go back to spawn.");
        if (!FFA.getPlugin().isEnabled()) {
            return;
        }
        Bukkit.getScheduler().runTaskLater(FFA.getPlugin(), () -> FFA.getInstanceManager().purgeInstance(this), 20 * 10L);
    }

    // start the game
    private void startGame() {
        pvpEnabled = true;
    }

    // save player score tallies
    private void saveScoreTallies(Player player) {
        ScoreManager sm = GameCore.getScoreManager();
        if (playerKills.get(player) != null) {
            sm.modifyScore(player, FFA.getFFAGame(), "Kills", playerKills.remove(player), ScoreFunction.ADD);
        }
        if (playerDeaths.get(player) != null) {
            sm.modifyScore(player, FFA.getFFAGame(), "Deaths", playerDeaths.remove(player), ScoreFunction.ADD);
        }
    }

    // check if the win condition has been met. If so end the game.
    private void gameWinCheck() {
        if (gamePlayers.size() <= 1 && gameStarted && respawnPlayers.size() == 0) {
            if (!gameEnded) {
                gameEnded = true;
                if (gamePlayers.isEmpty()) {
                    BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GOLD + "" + ChatColor.BOLD + "Nobody won the match.");
                } else {
                    for (Player player : gamePlayers) {
                        if (respawnTimers.containsKey(player)) {
                            respawnTimers.remove(player).stopTask();
                        }
                        BroadcastWorld.broadcastChat(getGameWorld().getBukkitWorld(), ChatColor.GREEN + "" + ChatColor.BOLD + player.getName() + " has won the match!");

                        if (GameCore.getScoreManager().isEnabled()) {
                            GameCore.getScoreManager().modifyScore(player, FFA.getFFAGame(), "Wins", 1, ScoreFunction.ADD);
                            int wins = Math.round(GameCore.getScoreManager().getScore(player, FFA.getFFAGame(), "Wins").get());
                            player.sendMessage(ChatColor.YELLOW + "Your wins have increased by 1");
                            player.sendMessage(ChatColor.GOLD + "You now have " + (wins) + ChatColor.GOLD + " wins!");
                        }
                        player.sendMessage(ChatColor.DARK_GRAY + "You had " + ((playerKills.get(player) == null)? 0:playerKills.get(player))
                                + " kills and " + ((playerDeaths.get(player) == null)? 0:playerDeaths.get(player)) + " deaths.");
                        spectatePlayerPrep(player);
                    }
                }
                endGameCountdown();
            }
        }
    }

    // refresh the gameWorld status
    private void worldStatusRefresh() {
        if (gameStarted) {
            getGameWorld().setStatus(GameStatus.RUNNING_CLOSED);
            return;
        }

        if (getPlayerCount() >= maxPlayers) {
            getGameWorld().setStatus(GameStatus.WAITING_CLOSED);
        } else {
            getGameWorld().setStatus(GameStatus.WAITING_OPEN);
        }
        getGameWorld().setPlayerCount(getPlayerCount());
        worldDespawnTimer();
    }

    // start the world despawn time if conditions are met
    private void worldDespawnTimer() {
        if (!FFA.getPlugin().isEnabled()) {
            return;
        }
        if (worldDespawnTimer == null || worldDespawnTimer.isCancelled()) {
            if (getPlayerCount() == 0) {
                worldDespawnTimer = Bukkit.getScheduler().runTaskLater(FFA.getPlugin(), () -> {
                    if (getPlayerCount() == 0) {
                        FFA.getInstanceManager().purgeInstance(this);
                    }
                }, 20 * FFA.getSettings().getWorldDespawnTime());
            }
        } else if (worldDespawnTimer != null && !worldDespawnTimer.isCancelled() && getPlayerCount() != 0) {
            worldDespawnTimer.cancel();
        }
    }

    // getters
    public Set<Player> getWaitingPlayers() {
        return waitingPlayers;
    }

    public Set<Player> getGamePlayers() {
        return gamePlayers;
    }

    public Set<Player> getDeadPlayers() {
        return deadPlayers;
    }

    public boolean pvpEnabled() {
        return pvpEnabled;
    }

    private Location getRandomSpawn() {
        for (int u = 0; u < 50; u++) {
            List<Region> spawnRegions = FFA.getSettings().getMapConfig(getGameWorld().getMap()).getSpawnRegions();
            Random rnd = GameCore.getGlobalRandom().getRandom();
            if (spawnRegions.isEmpty()) {
                return FFA.getSettings().getMapConfig(getGameWorld().getMap()).getGameSpawn();
            }
            Region selectedReg = spawnRegions.get(rnd.nextInt(spawnRegions.size()));
            int selX = selectedReg.getPosition1().getBlockX() + rnd.nextInt((int) selectedReg.getWidth());
            int selZ = selectedReg.getPosition1().getBlockZ() + rnd.nextInt((int) selectedReg.getDepth());
            for (int i = 0; i < (selectedReg.getHeight()+1); i++) {
                Block block = getGameWorld().getBukkitWorld().getBlockAt(selX, selectedReg.getPosition1().getBlockY()+i, selZ);
                Block upBlock = block.getRelative(BlockFace.UP);
                Block downBlock = block.getRelative(BlockFace.DOWN);
                if (downBlock.getType().isSolid()) {
                    if (upBlock.isPassable() && block.isPassable() && !(block.getType().equals(Material.FIRE) || block.getType().equals(Material.LAVA))) {
                        return new Location(getGameWorld().getBukkitWorld(), selX + 0.5f, selectedReg.getPosition1().getY()+i, selZ + 0.5f);
                    }
                }
            }
        }
        Bukkit.getLogger().info("FFA could not find spawn location for player.");
        return FFA.getSettings().getMapConfig(getGameWorld().getMap()).getSpawnRegions().get(0).getPosition1().getLocation();
    }
}
