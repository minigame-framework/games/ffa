package com.rast.ffa;

import com.rast.gamecore.*;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;

public class FFAGame extends Game {

    public FFAGame(String name, List<String> mapList, boolean canJoinWhenRunning, JavaPlugin plugin) {
        super(name, mapList, canJoinWhenRunning, plugin);
    }

    @Override
    public String getChatFormat(Player player) {
        if (player.getGameMode().equals(GameMode.SPECTATOR)) {
            return FFA.getSettings().getLocalChatFormat().replace("%color%", ChatColor.GRAY.toString());
        }
        return FFA.getSettings().getLocalChatFormat().replace("%color%", ChatColor.RED.toString());
    }

    @Override
    public GameInstance getNewGameInstance(GameWorld gameWorld) {
        return new FFAInstance(gameWorld);
    }
}
